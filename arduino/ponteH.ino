#define BRAKEVCC 0
#define CW 1
#define CCW 2
#define BRAKEGND 3
#define CS_THRESHOLD 15   // Definição da corrente de segurança (Consulte: "1.3) Monster Shield Exemplo").
// https://portal.vidadesilicio.com.br/hc-sr04-sensor-ultrassonico/ biblioteca
//Leitura de distância com o sensor HC-SR04
#include <Ultrasonic.h>
 Ultrasonic ultrassom(11,10); // define o nome do sensor(ultrassom)
//e onde esta ligado o trig(8) e o echo(7) respectivamente
 
long distancia;
 

int inApin = 2; // INA: Sentido Horário Motor0 e Motor1 (Consulte:"1.2) Hardware Monster Motor Shield").
int inBpin = 3; // INB: Sentido Anti-Horário Motor0 e Motor1 (Consulte: "1.2) Hardware Monster Motor Shield").
int inApinDireita = 4;
int inBpinDireita = 5;
int pwmpin1 = 6;    
int pwmpin2 = 7;// Entrada do PWM
int pwmpin3 = 8;
int pwmpin4 = 9;
int statpin = 13;
int i=0;
int comando;

void aceleraEsquerda(){
  //roda esquerda
            digitalWrite(inApin, HIGH);
            digitalWrite(inBpin, HIGH);
             digitalWrite(7, HIGH);
            digitalWrite(6, HIGH);
            delay(1000);
            digitalWrite(inApin, LOW);
            digitalWrite(inBpin, LOW);
  
  }

void aceleraDireita(){
              digitalWrite(inApinDireita, HIGH);
            digitalWrite(inBpinDireita, HIGH);
             digitalWrite(8, HIGH);
            digitalWrite(9, HIGH);
            delay(1000);
            digitalWrite(inApin, LOW);
            digitalWrite(inBpin, LOW);
}

void para(){
                      
            digitalWrite(inApinDireita, LOW);
            digitalWrite(inBpinDireita, LOW);
            digitalWrite(8, LOW);
            digitalWrite(9, LOW);
            delay(1000);
            digitalWrite(inApin, LOW);
            digitalWrite(inBpin, LOW);
}

void setup()                         // Faz as configuração para a utilização das funções no Sketch
{
Serial.begin(9600);              // Iniciar a serial para fazer o monitoramento
    pinMode(inApin, OUTPUT);
    pinMode(inBpin, OUTPUT);
    pinMode(inApinDireita, OUTPUT);
    pinMode(inBpinDireita, OUTPUT);

   pinMode(6, OUTPUT);
   pinMode(7, OUTPUT);
   pinMode(8, OUTPUT);
   pinMode(9, OUTPUT);
    
  Serial.println("Digite um numero: ");
  
  
    comando = Serial.read();

  
}

void loop()  {                        // Programa roda dentro do loop
            digitalWrite(inApin, HIGH);
            digitalWrite(inBpin, HIGH);
             digitalWrite(7, HIGH);
            digitalWrite(6, HIGH);
            
    

            
              digitalWrite(inApinDireita, HIGH);
            digitalWrite(inBpinDireita, HIGH);
             digitalWrite(8, HIGH);
            digitalWrite(9, HIGH);
            
   distancia = ultrassom.Ranging(CM);// ultrassom.Ranging(CM) retorna a distancia em
                                     // centímetros(CM) ou polegadas(INC)
   Serial.print(distancia); //imprime o valor da variável distancia
   Serial.println("cm");
   delay(100);

   if(distancia < 10){
       para();
   }

/*
    if(comando == 1){
// gira pra esquerda 
    aceleraDireita();
    }
    if(comando == 2){
// gira pra direita
    aceleraEsquerda() ;
    }
    if(comando == 3){
// vai pra frente

  aceleraDireita();
  aceleraEsquerda();
    }

    if(comando == 4){
// para
    para(); }
*/
    
  }
